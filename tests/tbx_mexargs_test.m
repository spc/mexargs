classdef tbx_mexargs_test < matlab.unittest.TestCase
% TBX_MEXARGS_TEST test class for C MEX-files argument checks
%
% Uses the mexargs_test class to construct the test case and define
% the test methods. Defines the mexfile property with all MEX-file names
% and each corresponding function defining the checks

%    Copyright 2024 Swiss Plasma Center EPFL
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%       http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.

%% begin mexargs_test boilerplate

  properties
    obj
  end

  methods (TestClassSetup)
    function setup_mexargs(testCase,mexfile)
      % Setup paths to mexargs project
      testCase.applyFixture(matlab.unittest.fixtures.PathFixture(testCase.mexargs_path));
      % Initialize mexargs object for the current MEX-file
      S = testCase.([mexfile,'_args']);
      % Initialize mexargs_test object with test methods
      testCase.obj = mexargs_test(S);
    end
  end

  methods (Test)
    function check_default(testCase)
      testCase.obj.check_default(testCase);
    end
    function check_nargin(testCase)
      testCase.obj.check_nargin(testCase);
    end
    function check_arguments(testCase)
      testCase.obj.check_arguments(testCase);
    end
    function check_nargout(testCase)
      testCase.obj.check_nargout(testCase);
    end
  end

%% end   mexargs_test boilerplate

  properties
    mexargs_path = fileparts(fileparts(mfilename('fullpath')));
  end

  properties (ClassSetupParameter)
    mexfile = {
      'copydoublemex',...
      };
  end

  methods (TestClassSetup)
    function setup_path(testCase)
      % Setup path to example folder
      example_path = fullfile(fileparts(fileparts(mfilename('fullpath'))),'example');
      testCase.applyFixture(matlab.unittest.fixtures.PathFixture(example_path));
    end
  end

  methods (Static)
    function S = copydoublemex_args
      S = mexargs('copydoublemex',{'eq',1},{'le',1});
      S.addarg(rand(11,1),{'type','double'});
    end
  end
end
