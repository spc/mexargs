/*    Copyright 2024 Swiss Plasma Center EPFL
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.*/

# include "mex.h"
# include "mexargs.h"

void mexFunction(int nlhs,       mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{

# define MEXNAME copydoublemex

 /* out = copydoublemex(in); */

# define OUT pout[0]

# define IN  prhs[0] /* double */

 CHECK_NARGIN_EQ(1);

 CHECK_DOUBLE(IN);

 CHECK_NARGOUT_LE(1);

 mxArray *pout[1] = {NULL};

 OUT = mxDuplicateArray(IN);

 ASSIGN_PLHS;
}
