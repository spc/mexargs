/*    Copyright 2024 Swiss Plasma Center EPFL
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.*/

# ifndef MEXARGS_H

# define MEXARGS_H

# if defined MATLAB_MEX_FILE || defined OCTAVE_MEX_FILE

 # define mexargs_str(in) #in
 # define MEXARGS_STR(in) mexargs_str(in)

 /* nargin checks */
 # define CHECK_NARGIN_EQ(NARG) if (nrhs != NARG) mexErrMsgIdAndTxt(MEXARGS_STR(MEXNAME)":nargin",MEXARGS_STR(MEXNAME)" expects exactly "#NARG" arguments but got %d instead",nrhs)
 # define CHECK_NARGIN_LE(NARG) if (nrhs  > NARG) mexErrMsgIdAndTxt(MEXARGS_STR(MEXNAME)":nargin",MEXARGS_STR(MEXNAME)" expects at most "#NARG" arguments but got %d instead",nrhs)
 # define CHECK_NARGIN_GE(NARG) if (nrhs  < NARG) mexErrMsgIdAndTxt(MEXARGS_STR(MEXNAME)":nargin",MEXARGS_STR(MEXNAME)" expects at least "#NARG" arguments but got %d instead",nrhs)

 /* Type checks */
 # define CHECK_TYPE(ARG,ARGNAME,TYPE,CHECK) if (!CHECK(ARG)) mexErrMsgIdAndTxt(MEXARGS_STR(MEXNAME)":inputType",MEXARGS_STR(MEXNAME)" expects "ARGNAME" to be a "#TYPE" array but got %s instead",mxGetClassName(ARG))
 # define CHECK_NUMERIC(ARG) CHECK_TYPE(ARG,#ARG,numeric,mxIsNumeric)
 # define CHECK_DOUBLE(ARG) CHECK_TYPE(ARG,#ARG,double,mxIsDouble)
 # define CHECK_INT8(ARG) CHECK_TYPE(ARG,#ARG,int8,mxIsInt8)
 # define CHECK_INT32(ARG) CHECK_TYPE(ARG,#ARG,int32,mxIsInt32)
 # define CHECK_UINT32(ARG) CHECK_TYPE(ARG,#ARG,uint32,mxIsUint32)
 # define CHECK_LOGICAL(ARG) CHECK_TYPE(ARG,#ARG,logical,mxIsLogical)
 # define CHECK_REAL(ARG) if (!mxIsDouble(ARG) && !mxIsSingle(ARG)) mexErrMsgIdAndTxt(MEXARGS_STR(MEXNAME)":inputType",MEXARGS_STR(MEXNAME)" expects "#ARG" to be a double or single array but got %s instead",mxGetClassName(ARG));
 # define CHECK_TYPE_MATCH(ARG1,ARG2) if (mxGetClassID(ARG1) != mxGetClassID(ARG2)) mexErrMsgIdAndTxt(MEXARGS_STR(MEXNAME)":inputType",MEXARGS_STR(MEXNAME)" expects "#ARG1" to be of the same type as "#ARG2" (%s) but got %s instead",mxGetClassName(ARG2),mxGetClassName(ARG1));

 /* Size checks */
 # define CHECK_SCALAR(ARG) if (!mxIsScalar(ARG)) mexErrMsgIdAndTxt(MEXARGS_STR(MEXNAME)":inputSize",MEXARGS_STR(MEXNAME)" expects "#ARG" to be a scalar array")
 # define CHECK_SCALAR_OR_EMPTY(ARG) if (!mxIsScalar(ARG) && !mxIsEmpty(ARG)) mexErrMsgIdAndTxt(MEXARGS_STR(MEXNAME)":inputSize",MEXARGS_STR(MEXNAME)" expects "#ARG" to be a scalar or empty array")
 # define CHECK_NUMEL_MSG(ARG,SIZE,MSG) if (mxGetNumberOfElements(ARG) != SIZE) mexErrMsgIdAndTxt(MEXARGS_STR(MEXNAME)":inputSize",MEXARGS_STR(MEXNAME)" expects "#ARG" to be an array of size "#SIZE"(%d) but got %d instead"MSG,SIZE,mxGetNumberOfElements(ARG))
 # define CHECK_NUMEL(ARG,SIZE) if (mxGetNumberOfElements(ARG) != SIZE) mexErrMsgIdAndTxt(MEXARGS_STR(MEXNAME)":inputSize",MEXARGS_STR(MEXNAME)" expects "#ARG" to be an array of size "#SIZE"(%d) but got %d instead",SIZE,mxGetNumberOfElements(ARG))
 # define CHECK_NROWS(ARG,SIZE) if (mxGetM(ARG) != SIZE) mexErrMsgIdAndTxt(MEXARGS_STR(MEXNAME)":inputSize",MEXARGS_STR(MEXNAME)" expects "#ARG" to be an array with "#SIZE"(%d) lines but got %d instead",SIZE,mxGetM(ARG))
 # define CHECK_NCOLS(ARG,SIZE) if (mxGetN(ARG) != SIZE) mexErrMsgIdAndTxt(MEXARGS_STR(MEXNAME)":inputSize",MEXARGS_STR(MEXNAME)" expects "#ARG" to be an array with "#SIZE"(%d) columns but got %d instead",SIZE,mxGetN(ARG))

 /* nargout checks */
 # define CHECK_NARGOUT_LE(NARG) if (nlhs  > NARG) mexErrMsgIdAndTxt(MEXARGS_STR(MEXNAME)":nargout",MEXARGS_STR(MEXNAME)" expects exactly "#NARG" output arguments but got %d instead",nlhs)

 /* argout assignments (always assign plhs[0] to assign MATLAB's ans variable when no output arguments are used) */
 # define ASSIGN_PLHS for(int ilhs=0; ilhs<(nlhs ? nlhs : 1); ilhs++) {plhs[ilhs] = pout[ilhs];}

# endif

# endif 
