#    Copyright 2024 Swiss Plasma Center EPFL
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Makefile.inc
# Define generic variables for Makefiles building MATLAB MEX-files

# Identify OS type
ifeq (,$(OS))
  # Discover operating system environment
  UNAME_S=$(shell uname -s)
  ifneq (,$(findstring inux,$(UNAME_S)))
    OS:=Linux
  endif
  ifneq (,$(findstring _NT,$(UNAME_S)))
    OS:=Windows
  endif
  ifneq (,$(findstring arwin,$(UNAME_S)))
    OS:=MacOS
  endif
endif
ifeq (Windows_NT,$(OS))
  # Note this will not work if the value is provided from the command line
  OS:=Windows
endif

ifneq (,$(VERBOSE))
  $(info INFO: OS=$(OS) origin=$(origin OS))
endif

ifeq (,$(filter Linux Windows MacOS,$(OS)))
  $(error Value of the OS variable should be one of Linux, Windows or MacOS but was $(OS). This variable can be overriden from the command line.)
endif

ifdef USE_OCTAVE
  MATPATH ?=/usr/bin
  MATBIN   =$(MATPATH)/octave
  MEXBIN   =$(MATPATH)/mkoctfile
  MEXEXT   =mex
  MATOPT   =--no-gui --eval
  MEXOPT   =--mex
  MEXCMD   =$(if $(filter-out default undefined,$(origin CC)),CC=$(CC) ,)XTRA_CFLAGS='$(MEX_CFLAGS) -DOCTAVE_MEX_FILE' $(if $(MEX_LDFLAGS),XTRA_LDFLAGS='$(MEX_LDFLAGS)' ,)$(MEXBIN) $(MEXOPT)
else
  # To choose a particular version of MATLAB/MEX
  #   define either the MATPATH or MATLAB variable
  #   to point to the root of your MATLAB installation
  #   i.e. $(MATPATH)/bin/matlab should exist

  # Set up MATLAB-related path
  ifeq (undefined,$(origin MATPATH))
    ifneq (,$(MATLAB))
      # If MATPATH is not explicitly defined and MATLAB is defined
      # then assign value of MATLAB to MATPATH
      MATPATH=$(MATLAB)
    endif
  endif
  ifeq (undefined,$(origin MATPATH))
    MATEXE =$(shell readlink -e $$(which matlab)) # Resolve links
    ifneq (,$(MATEXE))
      # IF MATPATH is not explicitly defined and there is a matlab
      # binary in the path then use it as base to define MATPATH
      ifneq (,$(wildcard $(dir $(MATEXE))/../bin/mex))
        # But first check that mex also exists in the same location
        MATPATH=$(abspath $(dir $(MATEXE))/..)
      endif
    endif
  endif
  MATPATH ?=/usr/local/MATLAB/R2019a
  MATBIN   =$(MATPATH)/bin/matlab
  MEXBIN   =$(MATPATH)/bin/mex
  MEXEXT   =$(shell $(MATPATH)/bin/mexext)
  MATOPT   =-nodisplay
  ifeq (Windows,$(OS))
    # Windows-specific paths
    MATPATH ?=/c/Progra~1/MATLAB/R2022b
    MATBIN   =$(MATPATH)/bin/matlab.exe
    MEXBIN   =$(MATPATH)/bin/win64/mex.exe
    MEXEXT   =$(shell $(MATPATH)/bin/mexext.bat)
    MATOPT   =-nodesktop -nosplash -wait
  endif
  ifeq (Linux,$(OS))
    # On LAC and similar machines, one has to add the license file manually
    LAC_FAMILY=lac lac2 crpppc204 lac4 lac5 lac6 lac7 lac8 lac9 lac10 crpppc282 spc-ci
    ifneq (,$(findstring $(shell hostname -s),$(LAC_FAMILY)))
      MATOPT+= -c /etc/network.lic
    endif
  endif
  MATOPT+= -r
  MEXCMD=$(MEXBIN) $(MEXOPT) $(if $(filter-out default undefined,$(origin CC)),CC=$(CC) ,)$(if $(MEX_CFLAGS),CFLAGS='$$CFLAGS $(MEX_CFLAGS)' ,)$(if $(MEX_LDFLAGS),LDFLAGS='$$LDFLAGS $(MEX_LDFLAGS)' ,)
endif

MATCMD=$(MATBIN) $(MATOPT)

# Determine type of build (cleaning only or not)
CLEAN_ONLY=no
ifneq (,$(MAKECMDGOALS))
  ifeq ($(MAKECMDGOALS),$(filter clean clear,$(MAKECMDGOALS)))
    CLEAN_ONLY=yes
  endif
endif

ifneq (,$(VERBOSE))
  $(info CLEAN_ONLY is $(CLEAN_ONLY))
endif

ifeq (no,$(CLEAN_ONLY))
  ifeq (,$(wildcard $(MEXBIN)))
    $(error Cannot find mex executable at location: $(MEXBIN))
  endif
  MEXEXT:=$(MEXEXT)
else
  MEXEXT:=
endif

ifneq (,$(VERBOSE))
  $(info MATPATH is $(MATPATH))
  $(info MATCMD  is $(MATCMD))
  $(info MEXCMD  is $(MEXCMD))
  $(info MEXEXT  is $(MEXEXT))
endif

# C Compiler support
ifeq (,$(findstring $(basename $(CC)),cc c99 gcc icc icx))
  $(error mexargs: Unsupported compiler: $(CC))
endif

# Determine compiler family
ifeq ($(basename $(CC)),$(filter $(basename $(CC)),cc c99 gcc))
  COMPILER_FAMILY=GNU
endif
ifeq ($(basename $(CC)),$(filter $(basename $(CC)),icc icx))
  COMPILER_FAMILY=Intel
endif

ifneq (,$(VERBOSE))
  $(info CC is $(CC))
  $(info COMPILER_FAMILY is $(COMPILER_FAMILY))
endif
