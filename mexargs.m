classdef mexargs < handle
% MEXARGS base class for interface description of MEX-file
%
% See also: mexargs_test

%    Copyright 2024 Swiss Plasma Center EPFL
%
%   Licensed under the Apache License, Version 2.0 (the "License");
%   you may not use this file except in compliance with the License.
%   You may obtain a copy of the License at
%
%       http://www.apache.org/licenses/LICENSE-2.0
%
%   Unless required by applicable law or agreed to in writing, software
%   distributed under the License is distributed on an "AS IS" BASIS,
%   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%   See the License for the specific language governing permissions and
%   limitations under the License.
  
 properties (SetAccess = public)
   mexname
   nargin
   nargout
   arg
 end

 methods
   function S = mexargs(mexname,nargin,nargout)
     S.mexname = mexname;
     S.nargin.check = nargin;
     S.nargout.check = nargout;
     S.arg = struct('default',{},'check',{});
   end

   function S = addarg(S,default,check)
     narg = numel(S.arg);
     S.arg(narg+1).default = default;
     S.arg(narg+1).check = check;
   end
 end
end
